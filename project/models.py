from django.db import models
from django.contrib.auth.models import User
from tf_model.models import TFModel


class Project(models.Model):
    name = models.CharField(max_length=250)
    details = models.TextField()
    user = models.ForeignKey(User, on_delete=models.CASCADE,
                             related_name='project')
    tf_models = models.ManyToManyField(TFModel, blank=True)

    def __str__(self):
        return self.name
