from django.urls import path
from . import views

app_name = 'project'

urlpatterns = [
    path('', views.ProjectIndexView.as_view(), name='index'),
    path('<int:pk>/', views.ProjectDetailView.as_view(), name='detail'),
    path('new/', views.ProjectCreateView.as_view(), name='new'),
    path('edit/<int:pk>/', views.edit, name='edit'),
    path('delete/<int:pk>/', views.delete, name='delete'),
]
