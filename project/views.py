from .models import Project
from django.views.generic import ListView, DetailView, CreateView
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse_lazy
from .forms import ProjectForm


class ProjectIndexView(ListView):
    template_name = 'project/index.html'
    context_object_name = 'project_list'

    def get_queryset(self):
        return Project.objects.filter(user=self.request.user)


class ProjectDetailView(DetailView):
    model = Project
    template_name = 'project/detail.html'


class ProjectCreateView(CreateView):
    model = Project
    template_name = 'project/new.html'
    form_class = ProjectForm
    form_valid_message = 'Project created successfully!'
    success_url = reverse_lazy('project:index')

    def get_initial(self):
        initial = super(ProjectCreateView, self).get_initial()
        initial.update({'user': self.request.user})
        return initial

    def form_valid(self, form):
        project = form.save(commit=False)
        project.user = self.request.user
        project.save()
        for tf_model in form.cleaned_data['tf_models']:
            project.tf_models.add(tf_model)
        return redirect('project:index')


def edit(request, pk, template_name='project/edit.html'):
    project = get_object_or_404(Project, pk=pk)
    form = ProjectForm(request.POST or None, instance=project)
    if form.is_valid():
        form.save()
        return redirect('project:index')
    return render(request, template_name, {'form': form})


def delete(request, pk, template_name='project/confirm_delete.html'):
    project = get_object_or_404(Project, pk=pk)
    if request.method == 'POST':
        project.delete()
        return redirect('project:index')
    return render(request, template_name, {'object': project})
