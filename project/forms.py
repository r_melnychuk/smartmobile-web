from django import forms
from django.forms.widgets import CheckboxSelectMultiple

from .models import Project, TFModel


class ProjectForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ProjectForm, self).__init__(*args, **kwargs)
        self.fields['tf_models'].widget = CheckboxSelectMultiple()
        user = self.initial.get('user', None) or self.instance.user
        self.fields['tf_models'].queryset = TFModel.objects.filter(user=user.id)

    class Meta:
        model = Project
        exclude = ['id', 'user']
