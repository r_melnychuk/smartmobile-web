from django.db import models
from django.contrib.auth.models import User


class DataSet(models.Model):
    TYPE_CHOICES = (
        ('text', 'text'),
        ('images', 'images'),
        ('audio', 'audio'),
    )
    name = models.CharField(max_length=250)
    description = models.TextField()
    user = models.ForeignKey(User, on_delete=models.CASCADE,
                             related_name='data_set')
    created = models.DateTimeField(auto_now_add=True)
    type = models.CharField(max_length=10, choices=TYPE_CHOICES, default='text')

    def __str__(self):
        return self.name
