from .models import DataSet
from django.views.generic import ListView, DetailView, CreateView
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse_lazy
from .forms import DataSetForm


class DataSetIndexView(ListView):
    template_name = 'dataset/index.html'
    context_object_name = 'dataset_list'

    def get_queryset(self):
        return DataSet.objects.filter(user=self.request.user)


class DataSetDetailView(DetailView):
    model = DataSet
    template_name = 'dataset/detail.html'


class DataSetCreateView(CreateView):
    model = DataSet
    template_name = 'dataset/new.html'
    form_class = DataSetForm
    form_valid_message = 'DataSet created successfully!'
    success_url = reverse_lazy('dataset:index')

    def form_valid(self, form):
        form = form.save(commit=False)
        form.user = self.request.user
        form.save()
        return redirect('dataset:index')


def edit(request, pk, template_name='dataset/edit.html'):
    dataset = get_object_or_404(DataSet, pk=pk)
    form = DataSetForm(request.POST or None, instance=dataset)
    if form.is_valid():
        form.save()
        return redirect('dataset:index')
    return render(request, template_name, {'form': form})


def delete(request, pk, template_name='dataset/confirm_delete.html'):
    dataset = get_object_or_404(DataSet, pk=pk)
    if request.method == 'POST':
        dataset.delete()
        return redirect('dataset:index')
    return render(request, template_name, {'object': dataset})
