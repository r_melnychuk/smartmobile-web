from django.urls import path
from . import views

app_name = 'dataset'

urlpatterns = [
    path('', views.DataSetIndexView.as_view(), name='index'),
    path('<int:pk>/', views.DataSetDetailView.as_view(), name='detail'),
    path('edit/<int:pk>/', views.edit, name='edit'),
    path('new/', views.DataSetCreateView.as_view(), name='new'),
    path('delete/<int:pk>/', views.delete, name='delete'),
]
