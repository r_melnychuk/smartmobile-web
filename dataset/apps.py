from django.apps import AppConfig


class DataSetConfig(AppConfig):
    name = 'dataset'
