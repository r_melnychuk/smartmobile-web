from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.models import User
from .forms import TFModelForm, VersionForm, FileForm
from .models import TFModel, ModelVersion, ModelFile
from django.views.generic import ListView, DetailView, CreateView


class IndexView(ListView):
    template_name = 'tf_model/index.html'
    context_object_name = 'tf_model_list'

    def get_queryset(self):
        return TFModel.objects.filter(user=self.request.user)


class VersionCreateView(CreateView):
    model = ModelVersion
    template_name = 'tf_model/new_version.html'
    form_class = VersionForm
    form_valid_message = 'Version created successfully!'

    def form_valid(self, form):
        form = form.save(commit=False)
        form.tf_model = get_object_or_404(TFModel, pk=self.kwargs['model_id'])
        form.save()
        return redirect('tf_model:detail', pk=self.kwargs['model_id'])


class TFModelDetailView(DetailView):
    model = TFModel
    template_name = 'tf_model/detail.html'

    # def get_context_data(self, **kwargs):
    #     context = super(TFModelDetailView, self).get_context_data(**kwargs)
    #     context['versions'] = self.object.model_versions.all()
    #     return context


class TFModelCreateView(CreateView):
    model = TFModel
    template_name = 'tf_model/new.html'
    form_class = TFModelForm
    form_valid_message = 'TF Model created successfully!'

    def get_initial(self):
        initial = super(TFModelCreateView, self).get_initial()
        initial.update({'user': self.request.user})
        return initial

    def form_valid(self, form):
        form = form.save(commit=False)
        form.user = self.request.user
        form.save()
        return redirect('tf_model:index')


class ModelFileView(CreateView):
    model = ModelFile
    template_name = 'tf_model/new_file.html'
    form_class = FileForm
    form_valid_message = 'Model file uploaded successfully!'

    def form_valid(self, form):
        form = form.save(commit=False)
        model_version = get_object_or_404(ModelVersion, pk=self.kwargs['version_id'])
        form.version = model_version
        form.save()
        return redirect('tf_model:detail', model_version.tf_model.id)


@login_required()
def edit(request, pk, template_name='tf_model/edit.html'):
    tf_model = get_object_or_404(TFModel, pk=pk)
    form = TFModelForm(request.POST or None, instance=tf_model)
    if form.is_valid():
        form.save()
        return redirect('tf_model:index')
    return render(request, template_name, {'form': form})


@login_required()
def delete(request, pk, template_name='tf_model/confirm_delete.html'):
    tf_model = get_object_or_404(TFModel, pk=pk)
    if request.method == 'POST':
        tf_model.delete()
        return redirect('tf_model:index')
    return render(request, template_name, {'object': tf_model})
