import uuid as uuid
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from dataset.models import DataSet


class TFModel(models.Model):
    TYPE_CHOICES = (
        ('img_cl', 'Image Classification'),
        ('custom', 'Custom'),
    )
    name = models.CharField(max_length=250)
    details = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE,
                             related_name='tf_models')
    dataset = models.OneToOneField(DataSet, null=True, blank=True, on_delete=models.SET_NULL)

    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    type = models.CharField(max_length=10, choices=TYPE_CHOICES, default='custom')

    def __str__(self):
        return self.name


class ModelVersion(models.Model):
    name = models.CharField(max_length=250, blank=False)
    tf_model = models.ForeignKey(TFModel, on_delete=models.CASCADE,
                                 related_name='model_versions')
    unique_together = ('tf_model', 'name')

    def __str__(self):
        return self.name


class ModelFile(models.Model):
    FORMAT_CHOICES = (
        ('tflite', 'tensorflow lite model'),
        ('pb', 'Custom'),
        ('h5', 'keras model'),
    )
    name = models.CharField(max_length=255, blank=False)
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)

    version = models.ForeignKey(ModelVersion, on_delete=models.CASCADE,
                                related_name='model_files')
    converted = models.BooleanField(default=False, editable=False)
    optimized = models.BooleanField(default=False, editable=False)
    format = models.CharField(max_length=10, choices=FORMAT_CHOICES, default='tflite')
    file = models.FileField()

    # path = models.FilePathField()

    def __str__(self):
        return self.name
