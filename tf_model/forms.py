from django import forms
from django.db.models import Q

from .models import TFModel, ModelVersion, ModelFile
from dataset.models import DataSet


class VersionForm(forms.ModelForm):
    class Meta:
        model = ModelVersion
        exclude = ('tf_model',)


class FileForm(forms.ModelForm):
    class Meta:
        model = ModelFile
        exclude = ('version',)


class TFModelForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(TFModelForm, self).__init__(*args, **kwargs)
        user = self.initial.get('user', None) or self.instance.user
        if self.instance.dataset:
            dataset_id = self.instance.dataset.pk
            self.fields['dataset'].queryset = DataSet.objects.filter(user=user.id).exclude(
                id__in=TFModel.objects.filter(Q(user=user.id) & ~Q(dataset_id__exact=dataset_id)).exclude(
                    dataset__isnull=True).values_list('dataset_id')
            )
        else:
            self.fields['dataset'].queryset = DataSet.objects.filter(user=user.id).exclude(
                id__in=TFModel.objects.filter(user=user.id).exclude(dataset__isnull=True).values_list('dataset_id')
            )
        # self.fields['dataset'].queryset = TFModel.objects.filter(user=user.id)

    class Meta:
        model = TFModel
        exclude = ('user',)
