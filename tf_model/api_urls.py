from django.urls import include, path
from rest_framework.routers import DefaultRouter
from . import api_views as api_views
from rest_framework_extensions.routers import NestedRouterMixin


class NestedDefaultRouter(NestedRouterMixin, DefaultRouter):
    pass


router = NestedDefaultRouter()
models_router = router.register('models', api_views.TFModelViewSet, 'models')
models_router.register(
    'versions',
    api_views.ModelVersionViewSet,
    basename='model-versions',
    parents_query_lookups=['tf_model']
).register(
    'files',
    api_views.ModelFileViewSet,
    basename='version-files',
    parents_query_lookups=['version__tf_model', 'version']
)

router.register('api_token_auth', api_views.AuthTokenViewSet, basename="api_token_auth")
# http post http://127.0.0.1:8000/api/api-token-auth/ username=vitor password=123

urlpatterns = [
    path('', include(router.urls)),
]
