from django.contrib import admin
from .models import TFModel, ModelVersion, ModelFile


# admin.site.register(TFModel)
#
# admin.site.register(ModelVersion)
# admin.site.register(ModelFile)
class ModelVersionAdminTabular(admin.TabularInline):
    model = ModelVersion


class TFModelAdmin(admin.ModelAdmin):
    list_display = ('name', 'created', 'uuid', 'type')
    list_filter = ('user',)
    inlines = [ModelVersionAdminTabular]


class ModelFileAdminTabular(admin.TabularInline):
    model = ModelFile
    readonly_fields = ["converted", "optimized"]


class ModelVersionAdmin(admin.ModelAdmin):
    inlines = [ModelFileAdminTabular]


admin.site.register(TFModel, TFModelAdmin)
admin.site.register(ModelVersion, ModelVersionAdmin)
admin.site.register(ModelFile)
