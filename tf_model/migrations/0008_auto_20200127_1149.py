# Generated by Django 3.0.2 on 2020-01-27 11:49

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('tf_model', '0007_auto_20200127_0943'),
    ]

    operations = [
        migrations.AlterField(
            model_name='modelfile',
            name='version',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='model_files', to='tf_model.ModelVersion'),
        ),
    ]
