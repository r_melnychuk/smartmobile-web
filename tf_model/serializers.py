from rest_framework import serializers
from .models import ModelFile, ModelVersion, TFModel


class ModelFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = ModelFile
        fields = ('pk', 'name', 'uuid', 'format', 'file', 'version')
        depth = 1


class ModelVersionSerializer(serializers.ModelSerializer):
    # model_files = ModelFileSerializer(many=True, read_only=True)

    class Meta:
        model = ModelVersion
        fields = ('pk', 'tf_model', 'name', 'model_files',)
        depth = 1


class TFModelSerializer(serializers.ModelSerializer):
    # model_versions = ModelVersionSerializer(many=True, read_only=True)

    class Meta:
        model = TFModel
        fields = ('pk', 'name', 'details', 'uuid', 'type', 'model_versions')
        depth = 1