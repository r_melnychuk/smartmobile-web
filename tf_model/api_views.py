from rest_framework import viewsets

from .serializers import ModelVersionSerializer, TFModelSerializer, ModelFileSerializer
from .models import ModelVersion, ModelFile, TFModel
from rest_framework.permissions import IsAuthenticated, BasePermission
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework import mixins
from rest_framework.renderers import JSONRenderer
from django.http import HttpResponse
from rest_framework_extensions.mixins import NestedViewSetMixin


class JSONResponse(HttpResponse):
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


class IsOwnModel(BasePermission):
    message = 'Not Authorized to access this resource/api - forbidden'

    def has_object_permission(self, request, view, obj):
        return obj.user == request.user


class TFModelViewSet(NestedViewSetMixin, viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, IsOwnModel,)
    serializer_class = TFModelSerializer
    queryset = TFModel.objects.all()

    def get_queryset(self):
        return TFModel.objects.filter(user=self.request.user)


class IsOwnVersion(BasePermission):
    message = 'Not Authorized to access this resource/api - forbidden'

    def has_object_permission(self, request, view, obj):
        return obj.tf_model.user == request.user


class ModelVersionViewSet(NestedViewSetMixin, viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, IsOwnVersion)
    serializer_class = ModelVersionSerializer
    queryset = ModelVersion.objects.all()
    # def get_queryset(self):
    #     return ModelVersion.objects.all()


class IsOwnFile(BasePermission):
    message = 'Not Authorized to access this resource/api - forbidden'

    def has_object_permission(self, request, view, obj):
        return obj.version.tf_model.user == request.user


class ModelFileViewSet(NestedViewSetMixin, viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, IsOwnFile,)
    queryset = ModelFile.objects.all()
    serializer_class = ModelFileSerializer


class AuthTokenViewSet(ObtainAuthToken, viewsets.GenericViewSet, mixins.CreateModelMixin,
                       mixins.ListModelMixin
                       ):
    def create(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)

    def list(self, request, *args, **kwargs):
        return JSONResponse(
            'post example http://127.0.0.1:8000/api/api-token-auth/ username=vitor password=123'
        )
