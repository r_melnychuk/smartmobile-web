from django.urls import path
from . import views

app_name = 'tf_model'

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('<int:pk>/', views.TFModelDetailView.as_view(), name='detail'),
    path('edit/<int:pk>/', views.edit, name='edit'),
    path('new/', views.TFModelCreateView.as_view(), name='new'),
    path('delete/<int:pk>/', views.delete, name='delete'),
    path('model/<int:model_id>/version/new', views.VersionCreateView.as_view(), name='new_version'),
    path('version/<int:version_id>/file/new', views.ModelFileView.as_view(), name='new_file'),
    path('model/<int:model_id>/version/new', views.VersionCreateView.as_view(), name='new_version'),
]
